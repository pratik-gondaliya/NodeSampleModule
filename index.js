function addition(x,y) {
    return x + y;
}


function subtraction(x,y) {
    return x - y;
}


function multiplication(x,y) {
    return x * y;
}

function division(x,y) {
    return x / y;
}

function echoMessage(val) {
    console.log('Consoled Message : ' + val);
};


module.exports = { addition: addition,subtraction:subtraction,multiplication:multiplication,division:division,echoMessage:echoMessage };