# pratikgondaliya Sample Module

*Use For Addition,Subtraction,Multiplication,Division.*

Dependency
==========
*npm install from the app direcoty.*
```groovy
npm install pratikgondaliya --save
```

### sample code
```node
var pratik = require('pratikgondaliya');

pratik.echoMessage('hello Pratik');
pratik.echoMessage("15 + 10 = " + pratik.addition(15,10));
pratik.echoMessage("15 - 10 = " + pratik.subtraction(15,10));
pratik.echoMessage("15 * 10 = " + pratik.multiplication(15,10));
pratik.echoMessage("15 / 10 = " + pratik.division(15,10));

```

Developed By
============

* Pratik Gondaliya - <gondaliya.p001@gmail.com>
 - Look me up on **StackOverflow**: [**Pratik Gondaliya**](http://stackoverflow.com/users/5944999/pratik-gondaliya)



License
=======

    Copyright 2018 Pratik Gondaliya

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.